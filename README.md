# Region Based Sales Platform

# Creating Database

- First of all create local mongodb database server.
- Create mongodb database named "Ecommerce".
- Then import json files provided in data folder into this database.


# Install Dependencies

Install Dependencies in both client and server folder by using :

`npm Install`

Then start the project by using command: 

`npm start`
